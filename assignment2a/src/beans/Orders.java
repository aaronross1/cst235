/*
 * Aaron Ross
 * CST235
 * 2/10/18
 * Work by Aaron Ross with help from tutorialspoint.com
 */
package beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean(name = "orders", eager = true)
@ViewScoped
public class Orders implements Serializable {
	private static final long serialVersionUID = 1L;
	private String orderNumber;
	private String orderName;
	private float price;
	private int quantity;
	private static final List<Order> orderList = new ArrayList<Order>(Arrays.asList(
		      new Order("09233", "geforce1060", (float)199.90, 2),
		      new Order("09234", "geforce1050ti", (float)129.90, 1),
		      new Order("09235", "geforce1080", (float)250.90, 1),
		      new Order("09236", "geforce1060", (float)159.90, 2)
		   ));; 
	
	public Orders() {

	}
	
	public synchronized List<Order> getOrderList() 
	{ 
		
		return orderList;
	}

	public String getName() {
		return orderNumber;
	}

	public void setName(String name) {
		this.orderNumber = name;
	}

	public String getDept() {
		return orderName;
	}

	public void setDept(String dept) {
		this.orderName = dept;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
