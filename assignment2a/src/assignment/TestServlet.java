package assignment;

import java.io.IOException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TestServlet
 * 
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private String firstName ="";
	private String lastName ="";
	
    /**
     * Default constructor. 
     */
    public TestServlet() {
    	firstName = "Aaron";
		lastName = "Ross";
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		System.out.println("String from init method");
		}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		System.out.println("String from destroy method");
		super.destroy();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		
		System.out.println("String from doGet method. My name is Aaron Ross.");
		response.getWriter().append("Served at: ").append(request.getContextPath());
		request.getRequestDispatcher("TestForm.jsp").forward(request, response);
		
		destroy();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String FirstName = request.getParameter("FirstName");
		String LastName = request.getParameter("LastName");
		
		if((FirstName.isEmpty() ||  FirstName == null) || LastName.isEmpty() || LastName == null)
		{
			request.setAttribute("error", "Incorrect entry");
			request.getRequestDispatcher("Error.jsp").forward(request, response);
		}
		else
		{
			request.setAttribute("FirstName", FirstName);
			request.setAttribute("LastName", LastName);
			request.getRequestDispatcher("Response.jsp").forward(request, response);
		}
		
	}
	
	public static void main(String args [])
	{
		TestServlet test = new TestServlet();
		try {
			test.init();
			
		}
		catch (ServletException e) {
			e.printStackTrace();
		}
	}

}
