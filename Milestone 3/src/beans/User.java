package beans;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean
@ViewScoped
public class User {
	@NotNull(message = "Enter first name.")
	@Size(min=3, max=25)
	private String firstName ="";
	@NotNull(message = "Enter password.")
	@Size(min=3, max=25)
	private String password="";
	
	public User() {
		firstName = "User";
		password = "";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String lastName) {
		this.password = lastName;
	}
	

	
}
